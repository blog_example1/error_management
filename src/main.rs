#[tokio::main]
async fn main() {
    let url = "https://jsonplaceholder.typicode.com/users/2";

    match user::run(url).await {
        Ok(user) => {
            println!("{user:?}");
        }
        Err(error) => {
            eprintln!("{error}")
        }
    }
}
