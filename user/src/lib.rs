use eyre::{Result, WrapErr};
use serde::Deserialize;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum UserError {
    #[error("Unable to fetch url: {0}")]
    Fetch(String),
    #[error("Unable to unmarshal user data: {0}")]
    Unmarshal(String),
    #[error("Unknown error")]
    Unknown,
}

#[derive(Deserialize, Debug)]
pub struct User {
    id: u32,
    name: String,
    email: String,
    phone: String,
    website: String,
}

async fn fetch_user(url: &str) -> Result<User> {
    let response = reqwest::get(url).await?;

    let response_string = response.text().await?;
    let user = serde_json::from_str(response_string.as_str())
        .wrap_err(format!("Error while trying to deserialize user from {url}"))?;

    Ok(user)
}

pub async fn run(url: &str) -> Result<User, UserError> {
    match fetch_user(url).await {
        Ok(user) => Ok(user),
        Err(report) => {
            if let Some(reqwest_error) = report.downcast_ref::<reqwest::Error>() {
                return Err(UserError::Fetch(reqwest_error.to_string()));
            }

            if let Some(serde_error) = report.downcast_ref::<serde_json::Error>() {
                if let Some(details) = report.downcast_ref::<String>() {
                    return Err(UserError::Unmarshal(details.to_string()));
                }
                return Err(UserError::Unmarshal(serde_error.to_string()));
            }

            Err(UserError::Unknown)
        }
    }
}
